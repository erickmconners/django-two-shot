from django.shortcuts import render, redirect
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


@login_required
def categories_list(request):
    categories_obj = ExpenseCategory.objects.filter()
    context = {
        "categories_obj": categories_obj,
    }
    return render(request, "guide/categories.html", context)


@login_required
def accounts_list(request):
    accounts_obj = Account.objects.filter()
    context = {
        "accounts_obj": accounts_obj,
    }
    return render(request, "guide/accounts.html", context)


@login_required
def receipt_list(request):
    receipt_obj = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_obj": receipt_obj,
    }
    return render(request, "guide/receipt.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form": form,
    }
    return render(request, "guide/create.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("categories_list")
    else:
        form = CategoryForm()
    return render(request, "guide/create_category.html", {"form": form})


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("accounts_list")
    else:
        form = AccountForm()
    return render(request, "guide/create_account.html", {"form": form})
