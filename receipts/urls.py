from django.urls import path
from receipts.views import (
    receipt_list,
    create_receipt,
    accounts_list,
    categories_list,
    create_category,
    create_account,
)


urlpatterns = [
    path("accounts/", accounts_list, name="accounts_list"),
    path("categories/", categories_list, name="categories_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
    path("", receipt_list, name="home"),
]
