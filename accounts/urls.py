from django.urls import path
from accounts.views import signup, logout_view, login_view

urlpatterns = [
    path("logout/", logout_view, name="logout"),
    path("login/", login_view, name="login"),
    path("signup/", signup, name="signup"),
]
